defmodule Chattr.Router do
  use Chattr.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug Chattr.Guardian.AuthPipeline
  end

  scope "/", Chattr do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/api", Chattr do
    resources "/users", UserController, only: [:create]
    post "/sessions", SessionController, :create

    pipe_through :api

    delete "/sessions", SessionController, :delete
    post "/sessions/refresh", SessionController, :refresh
    get "/users/:id/rooms", UserController, :rooms
    resources "/rooms", RoomController, only: [:index, :create] do
      resources "/messages", MessageController, only: [:index]
    end
    post "/rooms/:id/join", RoomController, :join
  end
end
