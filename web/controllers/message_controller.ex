defmodule Chattr.MessageController do
  use Chattr.Web, :controller

  plug Guardian.Plug.EnsureAuthenticated, handler: Chattr.SessionController

  def index(conn, params) do
    last_seen_id = params["last_seen_id"] || 0
    room = Repo.get!(Chattr.Room, params["room_id"])

    page =
      Chattr.Message
      |> where([m], m.room_id == ^room.id)
      |> where([m], m.id < ^last_seen_id)
      |> order_by([desc: :inserted_at, desc: :id])
      |> preload(:user)
      |> Chattr.Repo.paginate()

    render(conn, "index.json", %{messages: page.entries, pagination: Chattr.PaginationHelpers.pagination(page)})
  end
end
