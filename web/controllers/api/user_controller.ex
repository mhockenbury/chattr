defmodule Chattr.UserController do
  use Chattr.Web, :controller

  alias Chattr.User

  def create(conn, params) do
    changeset = User.registration_changeset(%User{}, params)

    case Repo.insert(changeset) do
      {:ok, user} ->
        new_conn = Chattr.Guardian.Plug.sign_in(conn, user)
        jwt = Chattr.Guardian.Plug.current_token(new_conn)

        new_conn
        |> put_status(:created)
        |> render(Chattr.SessionView, "show.json", user: user, jwt: jwt)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Chattr.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def rooms(conn, params) do
    current_user = Chattr.Guardian.Plug.current_resource(conn)
    rooms = Repo.all(assoc(current_user, :rooms))
    render(conn, Chattr.RoomView, "index.json", %{rooms: rooms})
  end
end
