defmodule Chattr.SessionController do
  use Chattr.Web, :controller

  def create(conn, params) do
    case authenticate(params) do
      {:ok, user} ->
        new_conn = Chattr.Guardian.Plug.sign_in(conn, user)
        jwt = Chattr.Guardian.Plug.current_token(new_conn)

        new_conn
        |> put_status(:created)
        |> render("show.json", user: user, jwt: jwt)
      :error ->
        conn
        |> put_status(:unauthorized)
        |> render("error.json")
    end
  end

  def delete(conn, _) do
    jwt = Chattr.Guardian.Plug.current_token(conn)
    Chattr.Guardian.revoke(jwt)

    conn
    |> put_status(:ok)
    |> render("delete.json")
  end

  def refresh(conn, _params) do
    user = Chattr.Guardian.Plug.current_resource(conn)
    jwt = Chattr.Guardian.Plug.current_token(conn)
    claims = Chattr.Guardian.Plug.current_claims(conn)

    case Chattr.Guardian.refresh(jwt) do
      {:ok, _old_stuff, {new_token, new_claims}} ->
        conn
        |> put_status(:ok)
        |> render("show.json", user: user, jwt: new_token)
      {:error, _reason} ->
        conn
        |> put_status(:unauthorized)
        |> render("forbidden.json", error: "Not authenticated")
    end
  end

  def unauthenticated(conn, _params) do
    conn
    |> put_status(:forbidden)
    |> render(Chattr.SessionView, "forbidden.json", error: "Not Authenticated")
  end

  defp authenticate(%{"email" => email, "password" => password}) do
    user = Repo.get_by(Chattr.User, email: String.downcase(email))

    case check_password(user, password) do
      true -> {:ok, user}
      _ -> :error
    end
  end

  defp check_password(user, password) do
    case user do
      nil -> Comeonin.Argon2.dummy_checkpw()
      _ -> Comeonin.Argon2.checkpw(password, user.password_hash)
    end
  end
end
