defmodule Chattr.Guardian do
  use Guardian, otp_app: :chattr

  alias Chattr.Repo
  alias Chattr.User

  def subject_for_token(resource, _claims) do
    {:ok, to_string(resource.id)}
  end

  def subject_for_token(_, _) do
    {:error, :reason_for_error}
  end

  def resource_from_claims(claims) do
    {:ok, Repo.get(User, String.to_integer(claims["sub"]))}
  end
  def resource_from_claims(_claims) do
    {:error, :reason_for_error}
  end
end

defmodule Chattr.Guardian.AuthPipeline do
  @claims %{typ: "access"}

  use Guardian.Plug.Pipeline, otp_app: :chattr,
    module: Chattr.Guardian,
    error_handler: Chattr.Guardian.AuthErrorHandler

  plug Guardian.Plug.VerifySession, claims: @claims
  plug Guardian.Plug.VerifyHeader, claims: @claims, realm: "Bearer"
  plug Guardian.Plug.EnsureAuthenticated
  plug Guardian.Plug.LoadResource, ensure: true
end
